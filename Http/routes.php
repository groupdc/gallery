<?php



Route::group(['middleware' => ['web']], function () {

	Route::group( array("prefix" => "admin", "as"=>"admin."), function() {

    	Route::group(['middleware' => 'auth:dcms'], function() {

    		//GALLERY
    		Route::group( array("prefix" => "gallery"), function() {
    			Route::any('api/table', array('as'=>'gallery.api.table', 'uses' => 'GalleryController@getDatatable'));
    		});
    		Route::resource('gallery','GalleryController');
    });
  });
});



 ?>

<?php

return [
    "Gallery" => [
        "icon"  => "far fa-image",
        "links" => [["route" => "admin/gallery/", "label" => "Gallery"]],
    ],
];

?>
